# Under Construction

[18/10/2019] Probably need to make the whole thing python3 compliant (which it isn't), will come soonish, hopefully...

[19/02/2020] Still having hope that I will do the move soon, but no one bugs me about this so I will assume that no one needs it
# HistFitter HepDataParser

Is frustration from the HepData encoding frustrating you up to no end? You also wished that the process was more automated and painless? You also wished you had a vodoo doll in those kind of hard times?

Well you have found the right place friend, come along and if you use histfitter, harness the power of the YieldsTable.py and join us in glorious HepData uploads

## Getting Started

To get started you just need to setup the ATLAS environment along with root, not needed at the moment, but time will come

### Prerequisites

- Python 2.7 
- ROOT


### Installing

No installation required, just check out the repository and you are gone

## Authors

* **Geoffrey Mullier** - *Bad buggy code* (allegedly)


## Acknowledgments

* A lot of thanks to Mike Flowerdew for his initial HepDataConverter

