import glob
import ast
import os
import sys
dep_folder = 'PyYAML-3.12'  # Adding dependencies for custom additional packages
dependencies = [os.path.join(dep_folder, dep) for dep in os.listdir(dep_folder)]  # Might want to filter a bit the parsing there

sys.path.extend(dependencies)

# sys.path.append("PyYAML-3.12")
# print sys.path
import yaml
import re
from ParserUtils import truncate
from ParserUtils import ListParser
# import GenerateThumbnail
from optparse import OptionParser
import ConfigParser

#Getting all the options from the Option.ini file parser
Config = ConfigParser.ConfigParser()
Config.read("Options.ini")
parser = OptionParser()

parser.add_option("--InputDir", help="Directorty to read from", default="Input/")
parser.add_option("--OutputDir", help="Output Directory", default="Output/")

# exit(0)

(options, args) = parser.parse_args()
# Global list that shall never be used, should not clutter memory too much in theory as long as the tables and histograms are not infinite
GlobalListAllFiles = []

#Getting the path from the option line or easier supposing that it didn't change
PathToInput = options.InputDir
PathToOutput = options.OutputDir

#Initialising LaTeX Dictionary for conversion of LaTeX terms when processing
LaTeXDictionary = {}
#Initialising comments as string variable in case
Comment = ""
#Initialising additional material in case
AdditionalMaterialDescription = ""
#Creating output submission file
OutputHeader = open(PathToOutput+'submission'+'.yaml', 'w')
OutputHeader.write('---\n')
#Veryfying if a LaTeX dictionary exists only start do parse it and convert it if it exists (literal_eval doesn't like empty strings)
try:
    LaTeXDictionary = Config.get('GlobalLaTeXConfig', 'LaTeXDictionary')
    if LaTeXDictionary:
        LaTeXDictionary = ast.literal_eval(LaTeXDictionary)
except ConfigParser.NoOptionError:
    pass
#Veryfing the header config and parsing the comment if there
try:
    Comment = Config.get('HeaderConfig', 'Comment')
except ConfigParser.NoOptionError:
    pass
#Parsing the addtional material if there is some trying to convert the list of dicitonaries to print it to the header
try:
    AdditionalMaterialDescription = Config.get('HeaderConfig', 'AdditionalMaterialDescription')
    OutputHeader.write("additional_resources:\n")
    if AdditionalMaterialDescription:
        AdditionalMaterialDescription = ast.literal_eval(AdditionalMaterialDescription)
        for DescriptionURL in AdditionalMaterialDescription:
            OutputHeader.write("- {description: '"+DescriptionURL["Description"]+"', location: '"+DescriptionURL["URL"]+"'}\n")
except ConfigParser.NoOptionError:
    pass
OutputHeader.write("comment: '"+Comment+"'\n")
# Finding all the tex files in the folder specified by input
# 
# Preparing to read all the input folder selecting only tex files, root files to come at some point, maybe, eventually...
for filename in glob.glob(os.path.join(PathToInput, '*.tex')):
    #Initialising variables just in case so python doesn't throw gang signs at me
    BinMin = 0.0
    BinStep = 1.0
    IsInteger = True
    IsHistogram = True
    VariableNameXAxis = ''
    VariableNameYaxis = ''
    UnitsXAxis = ""
    UnitsYAxis = ""
    BinList = []
    OutPutFilename = ""
    Cmenergies = 13000.0
    Description = ""
    Location = ""
    Image = ""
    Reactions = ""
    Observables = ""
    Phrases = ""
    Name = ""
    TableName = ""
    #Reading the configuration section, corresponding to the filename, if none is selected, the config defaults to the values above, some conversion take place for the sake of saveguarding humanity from errors.
    for section in Config.sections():
        if section in filename:
            try:
                BinMin = Config.get(section, 'BinMin')
                if BinMin:
                    BinMin = ast.literal_eval(BinMin)
            except ConfigParser.NoOptionError:
                pass
            try:
                BinStep = Config.get(section, 'BinStep')
                if BinStep:
                    BinStep = ast.literal_eval(BinStep)
            except ConfigParser.NoOptionError:
                pass
            try:
                IsInteger = Config.get(section, 'IsInteger')
                if IsInteger:
                    IsInteger = ast.literal_eval(IsInteger)
            except ConfigParser.NoOptionError:
                pass
            try:
                VariableNameXAxis = Config.get(section, 'VariableNameXAxis')
            except ConfigParser.NoOptionError:
                pass
            try:
                VariableNameYAxis = Config.get(section, 'VariableNameYAxis')    
            except ConfigParser.NoOptionError:
                pass
            try:
                UnitsXAxis = Config.get(section, 'UnitsXAxis')   
            except ConfigParser.NoOptionError:
                pass
            try:
                UnitsYAxis = Config.get(section, 'UnitsYAxis')   
            except ConfigParser.NoOptionError:
                pass
            try:
                IsHistogram = Config.get(section, 'IsHistogram')    
                if IsHistogram:
                    IsHistogram = ast.literal_eval(IsHistogram)
            except ConfigParser.NoOptionError:
                pass
            try:
                BinList = Config.get(section, 'BinList')
                if BinList:
                    BinList = ast.literal_eval(BinList)
            except ConfigParser.NoOptionError:
                pass
            try:
                Description = Config.get(section, 'Description')
            except ConfigParser.NoOptionError:
                pass
            try:
                Location = Config.get(section, 'Location')
            except ConfigParser.NoOptionError:
                pass
            try:
                Image = Config.get(section, 'Image')
            except ConfigParser.NoOptionError:
                pass
            try:
                Reactions = Config.get(section, 'Reactions')
            except ConfigParser.NoOptionError:
                pass
            try:
                Observables = Config.get(section, 'Observables')
            except ConfigParser.NoOptionError:
                pass
            try:
                TableName = Config.get(section, 'TableName')
            except ConfigParser.NoOptionError:
                pass
    #Two different lists, one that cleans totally special characters and the other that attempts to keep most of the LaTeX code safe, works more or less well...
    GlobalList = []
    GlobalListLaTeXIntact = []
    for line in open(filename, 'r'):
        #Uses only lines with & and with relevant data for fitted histogram and observed data, the implicit requirement is that a line of your table CANNOT be broken over several lines, HistFitter doesn't do it, be like HistFitter, be strong, don't brake your tables.
        #This version of the script does not take into account the multiline option, please refrain for using these otherwise mayhem and destruction will happen, probably...
        #Now has come also the time to introduce IsHistogram, both histograms and tables are treated differently because histograms do not need the first column (total number of events in the considered region, in this case the integral of the histogram)
        #If deactivated you default to the Table option
        if IsHistogram:
            #Testing if my histogram has observed or fitted, I didn't want  the rest, can be modified at your leasure
            if "&" and any(substring in line for substring in ["Observed","Fitted"]):
                #Cleans the string from all undesirable characters for parsing
                ReplaceList = {"$": "", "\pm": "+-","\\": "","\hline":"","[-0.05cm]":"",",":""} # define desired replacements here
                # use these three lines to do the replacement
                ReplaceList = dict((re.escape(k), v) for k, v in ReplaceList.iteritems())
                pattern = re.compile("|".join(ReplaceList.keys()))
                EpuredLine = pattern.sub(lambda m: ReplaceList[re.escape(m.group(0))], line)
                #Create a list out of the items in the table
                LineList = EpuredLine.split("&")
                #Keep an untouched copy of the original LaTeX formatting without linebreaks and hline in case
                ReplaceListMinimal = {"\\\\": "","\hline":"","[-0.05cm]":""} # 
                ReplaceListMinimal = dict((re.escape(k), v) for k, v in ReplaceListMinimal.iteritems())
                patternMinimal = re.compile("|".join(ReplaceListMinimal.keys()))
                EpuredLineMinimal = patternMinimal.sub(lambda m: ReplaceListMinimal[re.escape(m.group(0))], line)
                LineListMinimal = EpuredLineMinimal.split("&")
                LineListMinimal = [element.strip() for element in LineListMinimal]
                LineList = [element.strip() for element in LineList]
                GlobalList.append(LineList)
                GlobalListLaTeXIntact.append(LineListMinimal)
        else:
            #Table mode takes every line which is well formatted and removes the undesirable characters
            if "&" in line:
                #Cleans the string from all undesirable characters for parsing
                ReplaceList = {"$": "", "\pm": "+-","\\": "","\hline":"","[-0.05cm]":"",",":""} # define desired replacements here
                # use these three lines to do the replacement
                ReplaceList = dict((re.escape(k), v) for k, v in ReplaceList.iteritems())
                pattern = re.compile("|".join(ReplaceList.keys()))
                EpuredLine = pattern.sub(lambda m: ReplaceList[re.escape(m.group(0))], line)
                #Create a list out of the items in the table
                LineList = EpuredLine.split("&")
                #Keep an untouched copy of the original LaTeX formatting without linebreaks and hline in case
                ReplaceListMinimal = {"\\\\": "","\hline":"","[-0.05cm]":""} # 
                ReplaceListMinimal = dict((re.escape(k), v) for k, v in ReplaceListMinimal.iteritems())
                patternMinimal = re.compile("|".join(ReplaceListMinimal.keys()))
                EpuredLineMinimal = patternMinimal.sub(lambda m: ReplaceListMinimal[re.escape(m.group(0))], line)
                LineListMinimal = EpuredLineMinimal.split("&")
                LineListMinimal = [element.strip() for element in LineListMinimal]
                LineList = [element.strip() for element in LineList]
                GlobalList.append(LineList)
                GlobalListLaTeXIntact.append(LineListMinimal)
    #Remove path of the input and prepare for the output filename, if not specified in the config file, same name as input filename
    if OutPutFilename:
        OutputData = open(PathToOutput+OutPutFilename+'.yaml', 'w')
    else:
        OutputData = open(PathToOutput+filename.replace('.tex','.yaml').replace(PathToInput,''), 'w')
        #Again if specified finds the name of the X axis and the units if specified
    if IsHistogram:
        OutputData.write('independent_variables:\n')
        # OutputData.write("- header: {name: '"+VariableNameXAxis+"'}\n")
        if UnitsXAxis:
            OutputData.write("- header: {name: '"+VariableNameXAxis+"', units: '"+UnitsXAxis+"'}\n")
        else:
            OutputData.write("- header: {name: '"+VariableNameXAxis+"'}\n")
        # OutputData.write('- header: {name: Number of jets}\n')
        OutputData.write('  values:\n')
        #Need to make a difference for number of jets to be implemented
            #Writes the variables with the right stepping depending if it's an integer value or a bin with a certain width, if it is it will take the lowest Bin and go to the highest Bin by the steps specified, you still have to put explicitely what the stepping is in the Y axis of your Histogram...
        if BinList:
            for items in BinList:
                OutputData.write("    - {value: '"+str(items)+"'}\n")
        else:
            if IsInteger:
                for i in range(2, len(GlobalList[0])):
                    Bin = float(BinMin) + (i-2)*float(BinStep)
                    OutputData.write("    - {value: '"+str(Bin)+"'}\n")
            else:        
                for i in range(2, len(GlobalList[0])):
                    Low = float(BinMin) + (i-2)*float(BinStep)
                    High = float(BinMin) + (i-1)*float(BinStep)
                    OutputData.write('    - {low: '+str(Low)+', high: '+str(High)+'}\n')
    else:
        OutputData.write("independent_variables:\n")
        #if it's a table it will search for the name of the table, or give you a generic one
        if TableName:
            OutputData.write("- header: {name: '"+TableName+"'}\n")
            OutputData.write("  values:\n")
        else:
            OutputData.write("- header: {name: A table}\n")
            OutputData.write("  values:\n")
        for i in range(1, len(GlobalList)):
            # use these three lines to do the replacement with the LaTeX dictionary
            LaTeXDictionaryPass = LaTeXDictionary
            LaTeXDictionaryPass = dict((re.escape(k), v) for k, v in LaTeXDictionaryPass.iteritems())
            ReplacePattern = re.compile("|".join(LaTeXDictionaryPass.keys()))
            ReplacedLaTeX = ReplacePattern.sub(lambda m: LaTeXDictionaryPass[re.escape(m.group(0))], GlobalListLaTeXIntact[i][0])
            OutputData.write("    - {value: '"+ReplacedLaTeX+"'}\n")
        #Need to make a difference for number of jets to be implemented
    #  Core of the data ouput
    OutputData.write('dependent_variables:\n')
    if IsHistogram:
        for i in range(0, len(GlobalList)):
            if "Fitted" or "Observed" in GlobalList[i][0]:
                if UnitsYAxis:
                    OutputData.write("- header: {name: '"+VariableNameYaxis+"', units: '"+UnitsYAxis+"'}\n")
                else:
                    OutputData.write("- header: {name: '"+VariableNameYaxis+"'}\n")
            #Switching if table:
                # Get the luminosity as qualifier
                OutputData.write('  qualifiers:\n')
                OutputData.write("  - {name: '$\sqrt{s}$', units: GeV, value: "+str(Cmenergies)+"}\n")
                if 'bkg' in GlobalList[i][0]:
                    OutputData.write('  - {name: ., value: Total Bkg}\n')
                elif 'Observed' in GlobalList[i][0]:
                    OutputData.write('  - {name: ., value: Data}\n')
                else:
                    OutputData.write('  - {name: ., value: '+GlobalList[i][0].replace('Fitted','').replace('events','').strip()+'}\n')
                OutputData.write('  values:\n')
                for j in range(2, len(GlobalList[i])):
                    if 'Observed' in GlobalList[i][0]:
                        OutputData.write("  - {value: '"+GlobalList[i][j].strip()+"'}\n")
                    else:
                        if 'pm' in GlobalList[i][j]:
                            OutputData.write('  - errors:\n')
                            OutputData.write('    - {label: stat+syst, symerror: '+GlobalList[i][j].replace(' ','').split('pm')[1].strip()+'}\n')
                            OutputData.write('    value: '+GlobalList[i][j].replace(' ','').split('pm')[0].strip()+'\n')
                        elif '^' in GlobalList[i][j]:
                            if( GlobalList[i][j].replace(' ','').replace('{','').replace('}','').replace('_','').replace('^','').split('-')[1].split('+')[0] == '0.0') and (GlobalList[i][j].replace(' ','').replace('{','').replace('}','').replace('_','').replace('^','').split('-')[1].split('+')[1] == '0.0'):
                            # GlobalList[i][j].replace(' ','').replace('{','').replace('}','').replace('_','').replace('^','').split('-')[1].split('+')[0]
                                OutputData.write('  - errors:\n')
                                OutputData.write('    - {label: stat+syst, symerror: 0.0}\n')
                                OutputData.write('    value: '+GlobalList[i][j].replace(' ','').replace('{','').replace('}','').replace('_','').replace('^','').split('-')[0].strip()+'\n')
                            else:
                                OutputData.write('  - errors:\n')
                                OutputData.write("    - asymerror: {minus: -"+re.findall(r"\{\-(.*?)\}",GlobalList[i][j])[0]+", plus: "+re.findall(r"\{\+(.*?)\}",GlobalList[i][j])[0]+"}\n")
                                OutputData.write('      label: stat+syst\n')
                                OutputData.write('    value: '+GlobalList[i][j].replace(' ','').replace('{','').replace('}','').replace('_','').replace('^','').split('-')[0].strip()+'\n')    
    else:
        for j in range(1,len(GlobalList[i])):
            OutputData.write("- header: {name: .}\n")
            # OutputData.write("- header: {name: Number of events, units: Number of events}\n")
            OutputData.write("  qualifiers:\n")
            OutputData.write("  - {name: '$\sqrt{s}$', units: GeV, value: 13000}\n")
            LaTeXDictionaryPass = LaTeXDictionary
            LaTeXDictionaryPass = dict((re.escape(k), v) for k, v in LaTeXDictionaryPass.iteritems())
            ReplacePattern = re.compile("|".join(LaTeXDictionaryPass.keys()))
            ReplacedLaTeX = ReplacePattern.sub(lambda m: LaTeXDictionaryPass[re.escape(m.group(0))], GlobalListLaTeXIntact[0][j])
            OutputData.write("  - {name: ., value: '"+ReplacedLaTeX+"'}\n")
            OutputData.write("  values:\n")
            for i in range(1, len(GlobalList)):
                if "\pm" in GlobalListLaTeXIntact[i][j]:
                    OutputData.write("  - errors:\n")
                    OutputData.write("    - {label: stat+syst, symerror: "+GlobalListLaTeXIntact[i][j].replace("$","").replace(" ","").split("\pm")[1].strip()+"}\n")
                    OutputData.write("    value: '"+GlobalListLaTeXIntact[i][j].replace(" ","").replace("$","").split("\pm")[0].strip()+"'\n")
                elif "^" in GlobalList[i][j]:
                    if( GlobalList[i][j].replace(" ","").replace("{","").replace("}","").replace("_","").replace("^","").split("+")[1].split("-")[0] == "0.0") and (GlobalList[i][j].replace(" ","").replace("{","").replace("}","").replace("_","").replace("^","").split("+")[1].split("-")[1] == "0.0"):
                    # GlobalList[i][j].replace(" ","").replace("{","").replace("}","").replace("_","").replace("^","").split("-")[1].split("+")[0]
                        OutputData.write("  - errors:\n")
                        OutputData.write("    - {label: stat+syst, symerror: 0.0}\n")
                        OutputData.write("    value: '"+GlobalList[i][j].replace(" ","").replace("{","").replace("}","").replace("_","").replace("^","").split("-")[0]+"'\n")
                    else:
                        OutputData.write("  - errors:\n")
                        # OutputData.write("    - asymerror: {minus: -"+GlobalList[i][j].replace(" ","").replace("{","").replace("}","").replace("_","").replace("^","").split("+")[1].split("-")[1]+", plus: "+GlobalList[i][j].replace(" ","").replace("{","").replace("}","").replace("_","").replace("^","").split("+")[1].split("-")[0]+"}\n")
                        OutputData.write("    - asymerror: {minus: -"+re.findall(r"\{\-(.*?)\}",GlobalList[i][j])[0]+", plus: "+re.findall(r"\{\+(.*?)\}",GlobalList[i][j])[0]+"}\n")
                        OutputData.write("      label: stat+syst\n")
                        OutputData.write("    value: '"+GlobalList[i][j].replace(" ","").replace("{","").replace("}","").replace("_","").replace("^","").split("-")[0].split("+")[0]+"'\n")    
                else:
                    OutputData.write("  - {value: '"+GlobalList[i][j].replace("hline","").strip()+"'}\n")
    GlobalListAllFiles.append(GlobalList)
    OutputHeader.write('---\n')
    if OutPutFilename:
        OutputHeader.write("data_file: "+OutPutFilename+".yaml"+"\n")
    else:
        OutputHeader.write("data_file: "+filename.replace('.tex','.yaml').replace(PathToInput,'')+"\n")
    if Description:
        OutputHeader.write("description: "+Description+"\n")
    else:
        OutputHeader.write("description: Description comes here\n")
    OutputHeader.write("data_license: {description: null, name: null, url: null}\n")
    OutputHeader.write("keywords: \n")
    if Reactions:
        OutputHeader.write("- name: Reactions\n")
        OutputHeader.write("  values: ["+Reactions+"]\n")
    if Observables:
        OutputHeader.write("- name: Observables\n")
        OutputHeader.write("  values: ["+Observables+"]\n")
    if Phrases:
        OutputHeader.write("- name: Phrases\n")
        OutputHeader.write("  values: ["+Phrases+"]\n")
    OutputHeader.write("- name: Cmenergies\n")
    OutputHeader.write("  values: ["+str(Cmenergies)+"]\n")
    if Location:
        OutputHeader.write("location: '"+Location+"'\n")
    # else:
        # OutputHeader.write("location: 'Somewhere over the rainbow'\n")
    if Name:
        OutputHeader.write("name: '"+Name+"'\n")
    else:
        if OutPutFilename:
            OutputHeader.write("name: "+OutPutFilename+"\n")
        else:
            OutputHeader.write("name: "+filename.replace('.tex','').replace(PathToInput,'')+"\n")

print "Your HepData Files should be there!"
# exit(0)
            # OutputHeader.write("name: "+filename.replace('.tex','').replace(PathToInput,'')+"\n")

exit(0)
